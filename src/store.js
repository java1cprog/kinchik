import { writable, get, derived } from 'svelte/store';
import { goto } from '$app/navigation';

export const getRndFilms = async ([minYears = 1920, maxYears = new Date().getFullYear()]) => {
	const res = await fetch(`/api?min_years=${minYears}&max_years=${maxYears}`);
	const data = await res.json();
	const films = data.map((item) => {
		const img = new Image();
		img.src = item.src;
		return { ...item, img };
	});

	return films;
};

export const wordCount = writable([25]);
export const films = writable([]);
export const teams = writable([0, 0]);
export const filmIndex = writable(0);
export const teamIndex = writable(0);
export const team = derived([teams, teamIndex], ($teams, $teamIndex) => $teams[$teamIndex]);
export const years = writable([1920, new Date().getFullYear()]);
export const filterNumbers = writable(true);
export const turmMatchFilms = writable([]);
export const gameMatchFilms = writable([]);

function playAudio(id) {
	const audio = document.getElementById(id);
	if (audio) audio.play();
}

async function getFilms() {
	let filmsRaw = [];
	try {
		filmsRaw = await getRndFilms(get(years));
	} catch (err) {
		console.log(err);
		return getFilms(get(years));
	}
	const filmsFiltred = filmsRaw.filter((item) => {
		return (
			(get(filterNumbers) ? !/\d/.test(item.name) : true) &&
			item.name.length < 30 &&
			!get(films).some((filmItem) => filmItem.id === item.id)
		);
	});

	films.update((state) => {
		return [...state, ...filmsFiltred].slice(0, get(wordCount)[0]);
	});
	if (get(films).length === get(wordCount)[0]) {
		return;
	} else {
		return getFilms();
	}
}

export async function startRound() {
	goto('loading', { replaceState: true });
	films.set([]);
	turmMatchFilms.set([]);
	gameMatchFilms.set([]);
	teams.set([0, 0]);
	await getFilms();
	goto('startturn', { replaceState: true });
}

export function startTurn() {
	goto('turn', { replaceState: true });
}

export function match() {
	playAudio('guessAudio');

	films.update((state) => {
		turmMatchFilms.update((turmMatchFilmsState) => [...turmMatchFilmsState, state[0]]);
		return [...state.slice(1)];
	});
	teams.update((state) => {
		state[get(teamIndex)] += 1;
		return state;
	});
	if (get(films).length === 0) {
		playAudio('alarmAudio');
		goto('endgame', { replaceState: true });
	}
}

export function endTurn() {
	playAudio('alarmAudio');
	films.update((state) => {
		return [...state.slice(1), state[0]];
	});
	goto('endturn', { replaceState: true });
}

export function passTrun() {
	gameMatchFilms.update((state) => [...state, ...get(turmMatchFilms)]);
	turmMatchFilms.set([]);
	if (get(teamIndex) === 0) {
		teamIndex.set(1);
	} else {
		teamIndex.set(0);
	}
	goto('startturn', { replaceState: true });
}

export function newGame() {
	teamIndex.set(0);
	goto('/', { replaceState: true });
}
