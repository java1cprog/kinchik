export async function handle({ request, resolve }) {
	const response = ['/', '/rules', '/api'].includes(request.url.pathname)
		? await resolve(request)
		: { status: 303, headers: { location: '/' } };

	return response;
}
